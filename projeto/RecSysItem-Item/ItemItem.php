<?

mysql_connect("localhost", "root", "root") or
    die("Não foi possível conectar: " . mysql_error());
mysql_select_db("recsysitemitem");

//////////////////////////////////////////////////////////////////////
//Algoritmo de FC ITEM-ITEM
// Baseado no Artigo de:
// Amazon.com Recommendations Item-to-Item Collaborative Filtering
// Greg Linden, Brent Smith, and Jeremy York
// Por: Tiago Thompsen Primo
// http://www.tprimo.com
//////////////////////////////////////////////////////////////////////


RegistraGosto(3);

//$tokensA = array('Gremio, Brasil de Pelotas');
//$tokensB = array('Brasil de Pelotas', 'asdasdasda');

//echo "<BR>" . Similaridade($tokensA, $tokensB) . "<BR>";



function Similaridade($item1, $item2){
//Esta função recebe dois parâmetros em forma de string e retorna a similaridade
// de acordo com Calcula a similaridade entre duas strings como descrito em Oliver [1993]. 
// Note que esta implementação não usa um stack como no pseudo-código de Oliver, 
// mas chamadas recursivas as quais podem ou não tornar todo o processo mais rapido. 
//	Note também que a complexidade deste algotimo é O(N**3) aonde N é o tamanho da maior string.
	//similar_text($item1, $item2, $percent);
	$percent = cosineSimilarity($item1, $item2);

	return($percent);
}

function RecomendaItemItem($IdItem){
// Esta função vai ir na tabela de Similariade e recuperar todos os items
// que não o corrente ordenando pela coluna de similaridade.
// é importante mencionar que podem ser todos os itens, pois, só estarão nesta tabela
// aqueles itens marcados como de interesse do usuário.
// isto caracteriza uma FC.
}

function RegistraGosto($IdItem){
//Esta Função irá inserir na tabela matrizitemitemOliver
// OBS para um futuro update, esta tabela deve se chamar apenas matrizitemitem
// e a columa similaridade deve ter o nome da medida utilizada
// o item que algum usuário marcou como de seu interesse
// já computando a similaridade

	//Primeiro ver se a tabela é vazia
	$query = "Select count(*) as elementos, idItem1, idItem2 from matrizitemitemOliver";
	$result = mysql_query($query);
	while ($linha = mysql_fetch_array($result, MYSQL_ASSOC)) {
		if ($linha['elementos'] == 0) {
			echo "Insere o Item na Tabela sem similariade associada";
			$query2 = "INSERT INTO matrizitemitemOliver VALUES ($IdItem,NULL,NULL)";
			mysql_query($query2);
		}
		else{
			// Verificar se o único elemento é o mesmo que está entrando novamente'
			if ($linha['elementos'] == 1 && $linha['idItem1'] == $IdItem){
				echo "Não fazer nada pois o único elemento é mesmo do parâmetro de entrada";
			}
			else{
				if ($linha['elementos'] == 1 && $linha['idItem2'] == NULL){
					//calcular a similaridade da unica linha da base
					echo "Atualiza a similaridade da unica linha com o item";
					//echo "calcula a similaridade entre:" . $linha['idItem1'] . "e $IdItem";
					$keywordsItem = buscaKeywordsItem($IdItem);
					//print_r($keywordsItem);
					$keywordsItem2 = buscaKeywordsItem($linha['idItem1']);
					//print_r($keywordsItem2);
					$similaridade = Similaridade($keywordsItem,$keywordsItem2);
					//echo "valor da similaridade = " . $similaridade;
					$query6 = "update matrizitemitemOliver set idItem2 = $IdItem, similaridade = $similaridade where idItem1 = " . $linha['idItem1'];
					echo $query6;
					mysql_query($query6);
					
				}
				else{
					//Verificar se o item já está na matrizitemitemOliver
					$query3 = "select count(*) as achei from matrizitemitemOliver where idItem1 = $IdItem or idItem2 = $IdItem";
					echo $query3;
					$result2 = mysql_query($query3);
					while ($linha2 = mysql_fetch_array($result2)) {
						if ($linha2['achei'] > 0){
							echo "Nada precisa ser feito!";
						}
						else{
							echo "Atualizar a similaridade da tabela <BR>";
							//Primeiro eu vou pegar todos os elementos que já estão na tabela.
							// e coloca-los em 1 array.
							$query4 = "select * from matrizitemitemOliver";
							$result3 = mysql_query($query4);
							$elementos = array();
							while ($linha3 = mysql_fetch_array($result3)) {
								array_push($elementos, $linha3['idItem1'], $linha3['idItem2']);
							}
							//a variavel $elementos possui todos os itens que foram apontados como positivos por um usuário
							//preciso tirar os elementos repetidos para então calcular a similaridade com o item da entrada
							//print_r($elementos);
							//$elementosFiltrados = array_unique($elementos);
							$elementosFiltrados= array_keys(array_count_values($elementos));

							//print_r($elementosFiltrados);
							//Agora tenho que fazer a similaridade do Item de entrada com cada item dos elementosFiltrados
							$keywordsItem = buscaKeywordsItem($IdItem);
							//print_r($keywordsItem);
							for ($i=0;$i<count($elementosFiltrados);$i++){
								//echo "Silaridade do item $IdItem como o Item" . $elementosFiltrados[$i] . " = " . Similaridade($keywordsItem, buscaKeywordsItem($elementosFiltrados[$i])) . "<BR>";
								$similaridade = Similaridade($keywordsItem, buscaKeywordsItem($elementosFiltrados[$i]));
								$query5 = "insert into matrizitemitemOliver values ($IdItem,$elementosFiltrados[$i],$similaridade)";
								echo $query5 . "<BR>";
								mysql_query($query5);
							}
							

						}
					}
					
					
				}
			}
			

		}
	}


}


function cosineSimilarity($tokensA, $tokensB)
// Função que calcula a similaridade através de tradicional fórmula do coseno
// código obtido de: http://wpshack.com/comparing-two-text-similarity-in-php/
{
    $a = $b = $c = 0;
    $uniqueTokensA = $uniqueTokensB = array();

    $uniqueMergedTokens = array_unique(array_merge($tokensA, $tokensB));

    foreach ($tokensA as $token) $uniqueTokensA[$token] = 0;
    foreach ($tokensB as $token) $uniqueTokensB[$token] = 0;

    foreach ($uniqueMergedTokens as $token) {
        $x = isset($uniqueTokensA[$token]) ? 1 : 0;
        $y = isset($uniqueTokensB[$token]) ? 1 : 0;
        $a += $x * $y;
        $b += $x;
        $c += $y;
    }
    return $b * $c != 0 ? $a / sqrt($b * $c) : 0;
}

function buscaKeywordsItem($IdItem){
	$query = "select keywords from item where idItem = $IdItem";
	$resultado = mysql_query($query);
	while ($ponte = mysql_fetch_array($resultado)){
		//echo $ponte['keywords'];
		$keywords = preg_split("/[\s,]+/", $ponte['keywords']);
		//Cria um array com cada palavra em um elemento de 1 vetor. Importante para a função de coseno. 
		//print_r($keywords);
		return($keywords);
	}

}


?>