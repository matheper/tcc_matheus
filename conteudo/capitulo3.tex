\chapter{Sistemas de Recomendações}\label{cpSistemasRecomendacoes}

Atualmente a disponibilidade de informação na internet dificulta buscas precisas, a grande variedade de opções apresentadas a um indivíduo pode comprometer seu acesso ao conhecimento, muitas vezes este possui pouca experiência pessoal e não é capaz de discernir
entre as várias alternativas que lhe são apresentadas \citep{Reategui2005, Almazro2010}.

Em um ambiente de \aclp{CoP}, onde as interações são incentivadas e a produção de conteúdo facilitada,  a busca por informações específicas também pode ser desgastante. Neste cenário os sistemas de recomendação são ferramentas essenciais, que orientam e auxiliam o usuário a encontrar conteúdo relevante entre os itens disponíveis.


\section{Definições}

Sistemas de recomendação representam as preferências do usuário com o propósito de sugerir, de forma personalizada, novos itens de interesse. Eles são fundamentais para o comércio eletrônico e para o acesso a informação, sugerindo conteúdo, dentre um vasto espaço de possibilidades, que direcionam o usuário àqueles itens que melhor se enquadram em suas necessidades e preferências \citep{Burke2002}.

Segundo \citet{Cazella2010}, a capacidade de personalização de um sistema depende de um conjunto específico de funções. Toda a interação do usuário deve ser monitorada, a fim de coletar informações, como itens visualizados, interesses e preferências, que são a base para manter-se um modelo de usuário. O sistema pode apresentar, esconder ou enfatizar fragmentos de uma página, a fim de exibir somente as informações apropriadas, em um nível adequado de dificuldade ou detalhe, ou ainda modificar essa apresentação de modo a sugerir ao usuário seus próximos passos.

Na internet, a personalização de sistemas é largamente utilizada com o intuito de cativar o usuário. Os sistemas de recomendações são umas das ferramentas que colaboram com essa personalização, já que são capazes de identificar preferências e sugerir itens relevantes para cada usuário a partir da análise de seu comportamento de navegação, histórico de interação e preferências. 

Para que seja possível recomendar produtos, conteúdos ou pessoas a um usuário é necessário conhecê-lo. A eficiência das recomendações está diretamente relacionada à definição do perfil do usuário e à coleta de informações. Identificar qual o tipo de informação será relevante para a geração de recomendações é tão importante quanto capturá-la e armazená-la.

Segundo \apud{Barcellos2007}{Schafer2000} a estrutura dos sistemas de recomendação é dividida em quatro etapas: identificação do usuário, coleta de informações, estratégias de recomendação e visualização das recomendações (Figura \ref{estruturaSistemaRecomendacao}).

\FloatBarrier
\begin{figure}[!ht]
\centering
\caption{Estrutura do Sistema de Recomendação \citep{Schafer2000}.}
\includegraphics[width=0.8\textwidth,natwidth=600,natheight=600]{imagens/estruturaSistemaRecomendacao.png}
\label{estruturaSistemaRecomendacao}
\end{figure}
\FloatBarrier


O processo de identificação do usuário é opcional, porém, ao fazê-lo, é possível personalizar o sistema de acordo com as características daquele perfil. O perfil de um usuário expressa suas características, incluindo todas as informações diretamente solicitadas tal como as aprendidas durante sua interação no ambiente. O perfil é uma base de dados onde as informações sobre o usuário, incluindo seus interesses e preferencias, são armazenadas e pode ser dinamicamente mantido \citep{Cazella2010}.

A coleta de informações para a construção do perfil do usuário pode ser feita de forma explícita ou implícita \citep{Barcellos2007}. Na coleta explícita, o usuário indica o que lhe é importante, utilizando mecanismos como formulários, que exigem atenção, paciência e veracidade das informações fornecidas. Na forma implícita as informações são coletadas através do monitoramento das interações do usuário, páginas consultadas, tempo e frequência de visualização de conteúdo entre outros. O perfil do usuário é inferido com base no comportamento de outros usuários com padrão de comportamento similar ao seu.

As recomendações precisam ser exibidas ao usuário de maneira que possam ser facilmente visualizadas e compreendidas. Elas podem ser apresentadas através de e-mail, lista de recomendações, navegação, atendimento online e personificação \citep{Barcellos2007}.

Segundo \citet{Reategui2005} diferentes estratégias, com diferentes graus de complexidade, podem ser utilizadas para recomendar conteúdo aos usuários.

{\bf Listas de recomendação} - Mantém-se listas dos itens mais populares organizados por tipos de interesse, por exemplo, itens mais vendidos. Listas de recomendações são implementadas facilmente, mas suas sugestões não são dirigidas, o conteúdo a ser apresentado será o mesmo a todos, sem distinção de usuário.

{\bf Avaliações de usuários} - Após acessar um item o usuário pode avaliar o item. Comumente são apresentados ícones, que possibilitam a indicação de quanto aquele item é de interesse do indivíduo (Figura \ref{avaliacaoAmazon}). Do mesmo modo é possível representar seu interesse apenas de forma positiva, sem manter uma escala, assim como fazem redes sociais atuais como LinkedIn\footnote{http://www.linkedin.com/} (Gostei) e Facebook\footnote{http://www.facebook.com/} (Curtir). As avaliações também são importantes pois asseguram a qualidade e utilidade dos itens.
É a estratégia mais utilizada em sistemas de recomendações e sua implementação é simples, apenas sendo necessário armazenar e disponibilizar as avaliações de cada item, apresentando-os no momento apropriado.
\FloatBarrier
\begin{figure}[!ht]
\centering
\caption{Avaliações de usuários (estrelas) no site Amazon.com.}
\includegraphics[width=0.8\textwidth,natwidth=600,natheight=400]{imagens/avaliacaoAmazon.png}
\label{avaliacaoAmazon}
\end{figure}
\FloatBarrier

{\bf Individualização} - As recomendações são geradas especificamente para cada usuário levando em consideração suas preferências explícitas e/ou implícitas.

{\bf Correlação} - É baseada na associação de itens que tendem a serem acessados de maneira conjunta. A correlação também pode ser definida através de regras de associação, que demonstram como os itens se relacionam uns com os outros \citep{Barcellos2007}.

{\bf Associação por conteúdo} - Faz as recomendações baseada no conteúdo de determinado item, como um autor ou compositor, encontrando relações em um escopo mais restrito, por exemplo, dois filmes de determinado ator, que frequentemente são vendidos em conjunto.


\section{Técnicas de recomendação}
\label{tecnicasRec}

Segundo \citet{Burke2002} sistemas de recomendações possuem basicamente: dados de \textit{background}, que é a informação que o sistema possui antes de iniciar o processo de recomendação; dados de entrada, que é a informação que o sistema necessita para gerar as recomendações; e um algoritmo, que combina os dados de \textit{background} e de entrada para produzir suas sugestões. Identificando, deste modo,  cinco diferentes técnicas de recomendação como mostrado na Tabela \ref{tecnicasRecomendacoes}, onde {\bf I} é o conjunto de items sobre o qual as recomendações são feitas,  {\bf U} é o conjunto de usuários cujos as preferências são conhecidas, {\bf u} é o usuário para quem os itens são recomendados e {\bf i} é o item que a preferência de {\bf u} deve ser inferida.


\FloatBarrier
\begin{table}[!ht]
\caption{Técnicas de Recomendação \citep{Burke2002}}
\centering
\scriptsize
\begin{tabular}{|l|p{3cm}|p{3cm}|p{3.2cm}|}
\hline
\textbf{Técnica} & 
\textbf{Dados de \textit{Background}} & 
\textbf{Dados de Entrada} &
\textbf{Processo}
\\ \hline
\textbf{Colaborativa}
&Avaliações de {\bf U} de itens em {\bf I}.
&Avaliações de {\bf u} de itens em {\bf I}.
&Identificar os usuários em U similares a u, e a partir deles inferir suas avaliações de i
\\ \hline

\textbf{Baseada em Conteúdo}
&Características de itens em {\bf I}.
&Avaliações de {\bf u} de itens em {\bf I}.
&Gerar um classificador que ajuste o comportamento de avaliação de {\bf u} para usá-lo em {\bf i}.
\\ \hline

\textbf{Demográfica}
&As informações demográficas de {\bf U} e suas classificações de itens em {\bf I}.
&Informações demográficas de {\bf u}.
&Identificar os usuários que são demograficamente semelhantes a {\bf u}, e a partir deles inferir suas avaliações de {\bf i}.
\\ \hline

\textbf{Baseada em Utilidade}
&Características de itens em {\bf I}.
&Uma função de utilidade sobre os itens em {\bf I} que descrevem as preferências de {\bf u}.
&Aplicar a função aos itens e determinar a classificação de {\bf i}.
\\ \hline

\textbf{Baseada em Conhecimento}
&Características de itens {\bf I}. O conhecimento de como esses itens atendem às necessidades do usuário.
&Uma descrição de interesses e necessidades de {\bf u}.
&Inferir uma correspondência entre {\bf i} e as necessidades de {\bf u}.
\\ \hline

\end{tabular}
\label{tecnicasRecomendacoes}
\end{table}
\FloatBarrier

Além destas, \citet{Burke2002} identifica a filtragem híbrida que, a partir da combinação de duas ou mais técnicas, busca se beneficiar das qualidades e reduzir as limitações que cada uma pode apresentar. \citet{Reategui2004} ainda apresentam a técnica baseada em descritores de itens.


\subsection{Filtragem Colaborativa}

É a mais popular e madura das técnicas de recomendação. Importantes projetos de pesquisa, como Tapestry \citep{Goldberg1992} e GroupLens \citep{Sarwar1998}, assim como sites de comércio eletrônico como Amazon.com\footnote{http://www.amazon.com/} utilizam-se desta técnica.

A filtragem colaborativa tem em sua essência a troca de experiências entre usuários que possuem interesses comuns \citep{Cazella2010}. Os itens são filtrados levando-se em consideração avaliações feitas pelos usuários sobre um determinado assunto. A ideia básica é fazer com que participantes de determinada comunidade possam ser beneficiados pela experiência uns dos outros. Porém a maioria dos sistemas de filtragem colaborativa requer que os próprios usuários especifiquem o relacionamento de predição entre suas opiniões, ou indiquem os itens de interesse, ou indiquem pontuações dos itens pelos usuários \citep{Herlocker2000}. Um usuário de um sistema de filtragem colaborativa deve, portanto, pontuar cada item experimentado, indicando o quanto este item combina com sua necessidade de informação. A abordagem de filtragem colaborativa apresenta então uma média de pontuações para cada item com potencial de interesse. Assim, o sistema indica ao usuário itens que são considerados de interesse pelo grupo e evita os itens de menor interesse.

Em geral, nos sistemas tradicionais de filtragem colaborativa, a decisão sobre a recomendação baseia-se no histórico de avaliações comuns e no valor de predição previamente calculado. Tipicamente o perfil do usuário em um sistema colaborativo consiste em um vetor de itens e suas avaliações, que cresce conforme as interações do indivíduo com o sistema. Neste tipo de filtragem é possível identificar nichos de interesse distantes através do reconhecimento de similaridades entre usuários e a compreensão ou reconhecimento do conteúdo não é necessário.

Esta técnica pode apresentar limitações como: problema do primeiro avaliador, quando um novo item, por não ter sido avaliado, não é recomendado; pontuação esparsa, quando o número de usuários é pequeno em relação ao volume de informação; e problema de similaridade, quando determinado usuário tem preferências que variam do normal, dificultando a busca por usuários com gostos similares, o que gera recomendações pobres \citep{Reategui2005}.


\subsection{Filtragem Baseada em Conteúdo}

A filtragem baseada em conteúdo consiste em categorizar e definir associações entre itens. O item a ser recomendado é aquele que apresenta o maior nível de similaridade com as escolhas prévias do indivíduo.
Esta técnica é chamada de filtragem baseada em conteúdo porque o sistema realiza a filtragem baseada na análise de conteúdo do item e no perfil do usuário \citep{Cazella2006}.
O perfil de interesse do usuário é definido a partir de suas escolhas prévias e depende do método de aprendizagem empregado. Árvores de decisão, redes neurais e representações baseadas em vetores são utilizadas com esta finalidade \citep{Burke2002}.

A filtragem puramente baseada em conteúdo ignora a preferência de outros usuários e pode apresentar três limitações \citep{Adomavicius2005}:
a análise de conteúdo limitada, que é a dificuldade de categorizar itens pouco estruturados como imagens, áudio e vídeo;
a super especialização, que ocorre quando o sistema recomenda apenas itens semelhantes a itens avaliados positivamente, não apresentando conteúdo diferente do perfil do usuário, ou ainda, não apresentando conteúdo com alta similaridade a outros já recomendados, a fim de evitar redundâncias;
e o problema do novo usuário, que precisa avaliar um número suficiente de itens para que o sistema recomende novos conteúdos. Segundo \citet{Adomavicius2005}, novos usuário, com poucas avaliações, não seriam capazes de receber recomendações precisas.


\subsection{Filtragem Demográfica}

A filtragem demográfica visa categorizar o usuário a partir de seu perfil e então gerar recomendações baseado em sua classe demográfica.
O perfil do usuário é criado a partir da classificação dos usuários em estereótipos que representam as características de uma classe de usuários. As informações são coletadas através de formulários de registros e são utilizadas para a caracterização dos usuários e de seus interesses \citep{Reategui2005}.

O processo de recomendação consiste em cruzar os interesses de usuários da mesma categoria, encontrando itens ainda não avaliados pelo usuário alvo. A filtragem demográfica forma correlações entre pessoas assim como a filtragem colaborativa, mas requer menor esforço computacional pois não precisa raciocinar sobre os dados do usuário e tem claras as semelhanças entre os indivíduos.

\citet{Krulwich1997} utiliza esta técnica no sistema \textit{Prizm} para gerar recomendações para mais de 40,000 usuários. O sistema classifica a população dos Estados Unidos em 62 grupos demográficos a partir de seus históricos de compras e leva em consideração mais de 600 variáveis, cada uma caracterizando um estilo de vida, compra ou atividade.

O benefício desta abordagem é que ela não necessita de um histórico de avaliações do usuário, diferente da filtragem colaborativa e baseada em conteúdo, mas apresenta limitações como o problema de similaridade entre usuários.


\subsection{Filtragem Baseada em Utilidade}

A filtragem baseada em utilidade não se preocupa em construir uma generalização sobre o perfil de seus usuários, o foco está em definir uma função de utilidade que pode ser aplicada ao conjunto de itens disponíveis. Esta técnica gera as recomendações baseada na utilidade calculada a partir da função para cada item ao usuário. O problema central está em como criar a função para cada usuário. Tête-à-Tête e PersonaLogic \citep{Guttman1998}, sistemas que ajudam consumidores a identificar quais produtos melhor atendem suas necessidades, são exemplos de utilização desta técnica. O benefício da recomendação baseada em utilidade é que ela pode levar em consideração atributos externos aos itens, como confiabilidade do fornecedor e disponibilidade, recomendando, por exemplo, itens em liquidação a usuários com necessidades imediatas \citep{Burke2002}.


\subsection{Filtragem Baseada em Conhecimento}

A filtragem baseada em conhecimento tem ciência de como um item preenche determinada necessidade de determinado usuário, conhecimento funcional, podendo raciocinar sobre a relação entre uma necessidade e sua possível recomendação. O perfil do usuário pode ser qualquer estrutura de conhecimento que suporte esta inferência. Ele pode ser simplesmente uma consulta formulada pelo usuário como no site de buscas Google\footnote{https://www.google.com.br/}, porém esta representação, das necessidades do usuário, pode ser mais detalhada. O conhecimento utilizado também pode assumir diversas formas. Google utiliza informações sobre links entre páginas para inferir a popularidade e a relevância de um item.
Entretanto, os sistemas atuais não utilizam inferência, sendo necessário que o próprio usuário mapeie seus interesses.


\subsection{Filtragem Híbrida}

A filtragem híbrida procura aprimorar suas recomendações, através da combinação de duas ou mais abordagens, reduzindo suas limitações.
É comum a filtragem colaborativa ser utilizada em conjunto com alguma outra a fim de reduzir o problema do primeiro avaliador, mas diversas combinações são possíveis \citep{Burke2002}. Por exemplo, ao combinar as filtragens colaborativas e baseadas em conteúdo (Figura \ref{filtragemHibrida}) é possível: implementar os dois métodos separadamente e combinar suas predições; incorporar características do método baseado em conteúdo na implementação do método colaborativo; incorporar características do método colaborativo na implementação do método baseado em conteúdo; ou construir um modelo unificado, incorporando características tanto do método baseado em conteúdo quanto do método colaborativo \citep{Adomavicius2005}.

\FloatBarrier
\begin{figure}[!ht]
\centering
\caption{Filtragem Híbrida \citep{Cazella2006}.}
\includegraphics[width=0.7\textwidth,natwidth=600,natheight=350]{imagens/filtragemHibrida.png}
\label{filtragemHibrida}
\end{figure}
\FloatBarrier

\subsection{Descritores de Itens}


Um descritor de item mantém características demográficas e comportamentais dos usuários para construir seu perfil e inferir suas preferências. As interações dos usuários são monitoradas e suas sequências de acesso são armazenadas com o intuito de aprender sobre seus padrões de interação \citep{Reategui2005}.

Os descritores empregam parâmetros como fator de confiança (FConf) e fator de suporte (FSup) para indicar a força dos relacionamentos entre os itens.
Em uma associação X $\rightarrow$ Y o fator de suporte consiste na razão do número de usuários em que X e Y são verdadeiros, sobre o número total de usuários, enquanto que o fator de confiança consiste na razão do número de usuários em que X e Y são verdadeiros, sobre o número de usuários onde X é verdadeiro.

A figura \ref{descritoresItens}, utilizada como exemplo no assistente virtual Cadinho \citep{Reategui20052}, apresenta um descritor onde o item alvo é a geração da série de Fibonacci, classificada como ``Exercício'', e que tem ``Estrutura Para..Faça'', ``Cálculo da média dos números entre 8 e 80'' e ``Cálculo da soma números primos de 1 a 200'' como itens relacionados.


\FloatBarrier
\begin{figure}[!ht]
\centering
\caption{Exemplo de um descritor de item \citep{Reategui20052}.}
\includegraphics[width=0.75\textwidth,natwidth=600,natheight=400]{imagens/descritoresItens.png}
\label{descritoresItens}
\end{figure}
\FloatBarrier

A partir do fator de confiança é possível concluir que 70\% dos alunos que acessam o capítulo ``Estrutura Para..Faça'' buscam resolver o exercício de geração da série de Fibonacci e que 62\% de todos os alunos cadastrados acessam tanto o capítulo ``Estrutura Para..Faça'' quanto o exercício de geração da série de Fibonacci.

Para cada item a ser recomendado, o sistema cria um descritor e calcula a confiança entre o item alvo e os itens relacionados, provenientes da análise das interações do usuário. O sistema então calcula um \textit{score} para o descritor que pode variar entre diferente (0) e similar (1). Calculado o \textit{score} para cada descritor de item, aqueles que obtiverem o maior valor serão recomendados ao usuário.

\section{Conclusões sobre o capítulo}
%\subsection{Comparando as técnicas de recomendação}

Todas as técnicas analisadas durante o capítulo apresentam pontos fortes e fracos, dependendo do contexto onde são aplicadas. As vantagens e limitações são enumeradas a seguir.
%\newline

{\bf Vantagens}
\begin{enumerate}
\item Pode Identificar nichos de interesses diferentes dos já avaliados.
\item Conhecimento do domínio não é necessário.
\item Qualidade das recomendações cresce com o passar do tempo.
\item \textit{Feedback} implícito é suficiente.
\item Itens e usuários não precisam de avaliações anteriores.
\item Sensível a alterações de preferências.
\item Pode incluir características externas ao produto.
\item Pode mapear necessidades do usuário para os produtos.
\end{enumerate}

{\bf Desvantagens}
\begin{enumerate}
\setcounter{enumi}{8}
\item Problema do novo usuário, cujo perfil não pode ser avaliado por falta de informações.
\item Problema do novo item, que por possuir poucas avaliações não é recomendado.
\item Problema de similaridade do usuário, quando um indivíduo tem gostos diferentes do padrão, que dificulta a busca por usuários semelhantes.
\item Qualidade depende do tamanho do conjunto de itens avaliados.
\item Problema de estabilidade x maleabilidade, quando o perfil do usuário mantém preferências antigas, que podem não ser do interesse atual do indivíduo.
\item Necessita de informações demográficas.
\item Usuário deve especificar a função de utilidade.
\item Habilidade de sugestão é estática.
\item Construção do conhecimento é necessária.
\end{enumerate}

\newpage

As características de cada técnica são apresentadas de forma resumida na Tabela \ref{comparativoRecomendacoes}.

\FloatBarrier
\begin{table}[!ht]
\caption{Comparativo entre técnicas de recomendação (Adaptada de \citet{Burke2002}).}
\centering
\scriptsize
\begin{tabular}{|p{4cm}|p{4cm}|p{4cm}|}
\hline
\textbf{Técnica} & 
\textbf{Vantagem} & 
\textbf{Limitação}
\\ \hline
\textbf{Colaborativa}
& 1, 2, 3, 4
& 9, 10, 11, 12, 13
\\ \hline

\textbf{Baseada em Conteúdo}
&2, 3, 4
&9, 12, 13
\\ \hline

\textbf{Demográfica}
&1, 2, 3
&9, 11, 12, 13, 14
\\ \hline

\textbf{Baseada em Utilidade}
& 5, 6, 7
&15, 16
\\ \hline

\textbf{Baseada em Conhecimento}
&5, 6, 7, 8
&16, 17
\\ \hline

\textbf{Descritores de Itens}
&1, 3, 6, 8
&9, 10, 12, 14
\\ \hline

\end{tabular}
\label{comparativoRecomendacoes}
\end{table}
\FloatBarrier




Levando em consideração que o requisito básico da plataforma \ac{CoPPLA}, objeto deste trabalho, são as redes de relacionamento e os temas que conectam estas redes, apenas algumas características são importantes para nossa solução. Em relação aos itens de 1 a 4, a avaliação de outros interesses diferentes dos já avaliados é importante na medida que novos temas emergem nas interações dentro das comunidades; o conhecimento de domínio está implícito na comunidade e não precisa ser considerado em uma matriz de cálculos para recomendações; quanto mais proveitosas as recomendações, dando início a novas interações, melhor a sua qualidade na medida que as interações crescem em intervalos reduzidos de tempo.

As comunidades de prática focam principalmente nas trocas entre os parceiros e em quanto a necessidade de interação sobre determinado tema é inversamente proporcional ao tempo de aprendizagem em sua compreensão e aplicação, portanto não precisamos considerar os itens 7, 8 e 14 a 17.
%Os itens 7 e 8 também não serão considerados, uma vez que não há características externas aos itens que devem ser consideradas no momento da avaliação, assim como não deve haver a necessidade do usuário mapear seus interesses.
%Frase perigosa
%Os itens 6 e 13 referem-se a mudança de interesses dos usuários e a capacidade da técnica em adaptar-se a ela, porém no contexto de comunidades de prática, onde o domínio de interesse é bem definido durante seu ciclo de vida, está é uma característica que pode ser tolerada na escolha da técnica de recomendação.

A partir desta análise é possível identificar a técnica de filtragem colaborativa como sendo a que melhor se enquadra no contexto da aplicação de \textit{following} de \aclp{CoP}, proposta neste trabalho.
Através dela é possível gerar recomendações cruzadas, podendo sugerir itens completamente diferentes dos já avaliados, identificando usuários com preferências similares, declaradas explicitamente através do \textit{following}, e indicando itens ainda não avaliados uns aos outros. Nesta técnica também não é necessário o reconhecimento do conteúdo a ser recomendado nem a coleta de informações demográficas.
