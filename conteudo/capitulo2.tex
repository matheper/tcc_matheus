\chapter{Tecnologia de Comunidades de Prática}\label{cpComunidadesPratica}

O conceito \acl{CoP} é relativamente novo e pode ser definido como grupos de pessoas, que compartilham um interesse ou um conjunto de problemas e que aprendem através de interações regulares. A intenção de aprender em \aclp{CoP} pode não ser explícita, mas suas discussões, atividades e relacionamentos constroem uma história de aprendizagem conjunta.
O fenômeno de organizar-se em comunidades é antigo, pode-se observar sua formação em ambientes como escola, trabalho e atividades de lazer.
Grupos como vizinhos de um bairro, amigos que partilham interesse por determinado esporte ou colegas de trabalho podem ser considerados comunidades, mas segundo \citet{Wenger2008}, para que eles possam ser considerados \aclp{CoP}, seus elementos estruturais devem atender a três características fundamentais:

{\bf Domínio} - Uma rede de pessoas conectadas não define, por si só, uma \acl{CoP}. Ela deve possuir um propósito que orienta suas atividades e estabelece suas fronteiras, um domínio de interesses partilhado. O domínio também define a identidade de seus membros, uma vez que estes possuem competências que os diferem de pessoas que não fazem parte do grupo.

{\bf Comunidade} - Os membros, ao desenvolverem atividades e discussões em conjunto, estabelecem relacionamentos e compartilham informações. Estas relações proporcionam aos participantes aprender uns com os outros e são essenciais para a caracterização da comunidade.

{\bf Prática} - É um conjunto de esquemas de trabalho, ferramentas, ideias, estilos, linguagem, histórias e documentos decorrentes das interações na comunidade. A prática representa o conhecimento desenvolvido, compartilhado e armazenado, que torna-se fonte de conhecimento para seus membros e outros que possam se beneficiar destes recursos.


\newpage

Segundo \citet{Wenger2000}, para que as \aclp{CoP} sejam bem sucedidas é necessário que elas tenham a capacidade de serem sistemas de aprendizagem social.

\citet{Healy2009} define aprendizagem como o processo de evolução de um estado sem conhecimento para um onde há conhecimento. O aspecto social está nas interações humanas, aprender através do contato e associações regulares. A possibilidade de interagir e trocar experiências, fazendo com que os membros aprendam sobre um domínio e também sobre a própria participação, torna a \acl{CoP} apta a suportar a aprendizagem social, ao mesmo tempo que estimula o processo de participação e aprofundamento do domínio de aprendizagem \citep{Ribeiro2011}.

As relações e o sentimento de pertencer à comunidade são fundamentais no processo de aprendizagem social. A partir do momento que o participante adquire confiança e se identifica com seus pares estas relações se intensificam. Inicia-se um processo de motivação intrínseca, onde os participantes sentem-se a vontade para participar de discussões, expor suas ideias e socializar o conhecimento.

As \aclp{CoP} também apresentam um ciclo de vida (Figura \ref{estagiosComunidade}), onde as etapas não possuem tempo definido de duração, mas estão diretamente ligadas a participação e interesse de seus integrantes. As etapas são: potencial/criação, início/expansão, maturação, ativa e dispersão/transformação.

\FloatBarrier
\begin{figure}[!ht]
\centering
\caption{Ciclo de vida de \aclp{CoP} \citep{Wenger2005}.}
\includegraphics[width=0.9\textwidth,natwidth=600,natheight=600]{imagens/estagiosComunidade.png}
\label{estagiosComunidade}
\end{figure}
\FloatBarrier
\newpage

{\bf Potencial}/{\bf Criação} - Inicialmente pessoas com questões e necessidades similares formam redes imprecisas que, com o tempo, são agregadas e formam comunidades. Esta fase tem como principal objetivo constituir a comunidade em torno de seus três elementos estruturantes, domínio, comunidade e prática.

{\bf Início}/{\bf Expansão} - Em seguida existe um engajamento, por parte dos participantes, em atividades de aprendizagem que consolidam o grupo como comunidade, aumentando o grau de confiança e compreensão entre os indivíduos.

{\bf Maturação} - A comunidade se responsabiliza por suas práticas e expande-se, estipula-se padrões e define-se agendas, ritmo e identidade própria. O foco passa a ser o desenvolvimento do conhecimento de forma coletiva.

{\bf Ativa} - A comunidade passa a apresentar ciclos de atividades, onde a participação oscila entre picos altos e baixos. A \acl{CoP} já está estabelecida e possui conhecimento sólido, mas necessita estar aberta a novas ideias e novos participantes. Novos relacionamentos devem ser criados para que a comunidade continue ativa, mesmo durante períodos de baixa atividade.

{\bf Dispersão}/{\bf Transformação} - Por desenvolverem novos interesses ou a comunidade tratar de um assunto desatualizado ou sem grande relevância, os participantes acabam se distanciando da comunidade. Neste momento a comunidade pode sofrer transformações que geram outras comunidades ou se dispersar.

O sucesso de uma \acl{CoP} implica em compreender estas etapas e assegurar que as atividades e ferramentas atendam as necessidades de cada uma delas.


\section{Orientações e Atividades Colaborativas}


%Em \aclp{CoP} o aprendizado é construído de diferentes maneiras e o modo com que os participantes interagem define seu estilo. A \acl{CoP} pode apresentar mais de um estilo simultaneamente, e isso pode mudar ao longo do tempo. \citet{Wenger2010} definem nove diferentes orientações que \aclp{CoP} podem assumir, cada uma delas associada a um conjunto de ferramentas que proporcionam suporte ao padrão de suas atividades.

Em \aclp{CoP} o aprendizado é construído de diferentes maneiras e o modo com que os participantes interagem define seu estilo. A \acl{CoP} pode apresentar mais de um estilo simultaneamente, e isso pode mudar ao longo do tempo. Cada estilo ou orientação é associado a um conjunto de ferramentas que proporcionam suporte ao padrão de suas atividades.
\citet{Wenger2010} definem nove diferentes orientações que \aclp{CoP} podem assumir: encontros, conversas em aberto, projetos, conteúdo, acesso a perícia (\textit{expertise}), relacionamentos, participação individual, cultivar a própria comunidade e contribuir num contexto específico.


{\bf Encontros} - Algumas comunidades tem ênfase em encontros regulares, onde os participantes sobretudo compartilham atividades por algum tempo. A participação visível dos membros é o que define a existência da comunidade.
Flexibilidade suficiente na agenda para interações espontâneas, ritmo adequado de encontros, com frequência e agenda adequados e prática de encontros da comunidade são fatores importantes para o sucesso das comunidades orientadas a encontros.

As principais atividades desenvolvidas em \aclp{CoP} com esta orientação são: interações síncronas e assíncronas, \textit{feedback} para membros e tomada de decisão, participação à distância em encontros face a face, criação e distribuição de notas compartilhadas e/ou colaborativas para encontros online ou face a face.


{\bf Conversas em aberto} - Comunidades onde as conversações são as principais forma de aprendizagem, que caracterizam e mantém a comunidade unida.
Variedade suficiente de tópicos para manter o interesse, participação ativa de um segmento representativo da comunidade e arquivos bem organizados das conversações para evitar conversas circulares e ajudar iniciantes a se situarem são fundamentais para o sucesso deste estilo de comunidade.

Algumas das principais atividades desenvolvidas nestas comunidades são: conversas sobre tópicos, tradução entre línguas, desenvolvimento de subgrupos com controle de acesso, destaque de pontos chave de aprendizagem e arquivamento.



{\bf Projetos} - 
As comunidades são focadas em projetos, na resolução de projetos específicos ou na produção de artefatos úteis.
Normalmente os projetos envolvem subgrupos e os membros desenvolvem suas práticas juntos.
Estas comunidades devem apresentar uma definição coletiva dos projetos relacionados ao domínio da comunidade, definir coordenação, liderança e comunicação adequada entre subgrupos e o resto da comunidade.

A Criação de conteúdo em conjunto, desenvolvimento de subgrupos, gestão de projetos e comunicação com o restante das comunidades são algumas das atividades desenvolvidas em \aclp{CoP} com esta orientação.


{\bf Conteúdo} - 
Determinadas comunidades estão principalmente interessadas em criar, compartilhar e dar acesso a
documentos, ferramentas e outros conteúdos. O conteúdo bem organizado é um recurso importante para os membros, logo, a comunidade deve organizá-lo cuidadosamente, refletindo a visão da comunidade sobre seu domínio. Também deve apresentar uma taxonomia flexível que permita crescimento e evolução, facilidade de publicação interna ou para um público mais amplo e boa capacidade de busca.

As atividades características dessas comunidades são: \textit{upload} e compartilhamento de arquivos e documentos, comentários, anotações e discussão de conteúdo, acesso a conteúdo interno e externo, avaliação de contribuições e arquivamento.



{\bf Acesso a perícia (\textit{expertise})} - 
Este estilo tem como objetivo fornecer acesso ao conhecimento da comunidade, tanto internamente quando externamente. Essas comunidades são focadas em responder perguntas e de forma colaborativa resolver problemas. O conhecimento pode ser produzido e mantido por todo o grupo, ou por apenas um pequeno conjunto de peritos. O acesso a informação confiável ou às respostas dos peritos deve ser rápido, as pessoas devem saber onde encontrar o conhecimento. O grau de confiança de uma resposta é avaliado através da reputação de quem respondeu ou através de processos explícitos de validação. 

Perguntas e respostas, localizar perícia, validar ou avaliar respostas e promover questões ainda não respondidas ou com respostas inadequadas,
compartilhar problemas resolvidos e seguir peritos são algumas das atividades desenvolvidas nestas comunidades.



{\bf Relacionamentos} - 
As comunidades possuem foco no relacionamento construído entre seus membros, enfatizando o aspecto interpessoal da aprendizagem social.
Comunidades com essa orientação são voltadas a criar, sustentar e representar relacionamentos. A possibilidade de conhecer outras pessoas e proporcionar conexões informais entre elas são fatores importantes para este estilo de \acl{CoP}.

As principais atividades deste estilo são: relacionar e encontrar outros participantes, descobrir informações expressas na identidade pessoal de cada usuário, saber quem está conectado na comunidade, podendo interagir informalmente e seguir outros participantes.



{\bf Participação individual} - 
A aprendizagem, mesmo quando em grupo, acontece de forma individual. Pessoas carregam diferentes experiências, estilos de comunicação e aspirações quanto a sua participação na comunidade, o que torna o processo de aprendizagem heterogêneo. Em comunidades orientadas a participação individual os membros tem a possibilidade de desenvolver seu próprio estilo de participação e a comunidade acolhe, suporta e evolui com essa diversidade.

Personalização, assinaturas e outros mecanismos de alertas, disponibilizar modos de interação, gerenciar a participação individual publicamente, gerenciar a própria privacidade e prover suporte explícito para vários membros são atividades que caracterizam comunidades de participação individual.



{\bf Cultivar a própria comunidade} - 
Tem como objetivo evoluir de forma organizada e eficaz, proporcionando um ambiente favorável ao trabalho. As atividades da comunidade são bem planejadas e seus materiais de referência são bem produzidos e organizados.

\textit{Feedback} da comunidade, anúncios e outras informações enviadas diretamente aos membros, organização das interações e gratificações por empenho na comunidade são atividades características deste estilo.


{\bf Contribuir num contexto específico} - 
O objetivo central de comunidades que apresentam essa orientação é servir a um contexto específico que caracteriza a identidade e a forma como a comunidade opera. Elas podem ser utilizadas dentro de organizações, ter a missão de fornecer recursos de aprendizagem para o mundo ou buscar interações com outras comunidades cujo domínio complementa o seu próprio.

Criar interfaces, oferecendo conteúdo da comunidade para o exterior, constelações de comunidades relacionadas, convites públicos e recrutamento de membros são atividades presentes nesta orientação de comunidades. 

A próxima seção enfoca as ferramentas tecnológicas que, quando combinadas adequadamente, se tornam apropriadas a estilos específicos de \aclp{CoP}.


\section{Ferramentas de colaboração e comunicação}
As \aclp{CoP} utilizam ferramentas tecnológicas tradicionais para sua instrumentalização, alterando apenas seus objetivos e formas de utilização. A orientação da comunidade é fundamental para a definição de suas ferramentas básicas. Aquelas que não se enquadram ao estilo tornam-se ociosas, enquanto que a falta de outras, que deveriam estar presentes, desmotivam e limitam a participação no ambiente.


\citet{Wenger2005} sugere diversas ferramentas para a instrumentalização em \aclp{CoP}, desde ferramentas de comunicação tradicionais como chat, fórum de discussão e e-mail até ferramentas mais sofisticadas como vídeo-conferência e podcast. Também são necessárias ferramentas de gestão e publicação (Figura \ref{quadroAtividadesFerramentas}).
Sendo assim, a inovação não está nas ferramentas, mas sim no enfoque de seu uso, que permite aos membros da \acl{CoP} a disseminação do conhecimento \citep{Tavares2011}.

Visando definir possibilidades tecnológicas que atendam as necessidades dos membros de uma \acl{CoP}, \citet{Wenger2005} identifica as interfaces requeridas por meio de tensões presentes nas relações de troca entre os membros e a comunidade gerando três tipos de necessidades: Interação (síncrona e assíncrona), Publicação e Tendência (participação individual e cultivo da comunidade), ilustrado na Figura \ref{quadroAtividadesFerramentas}.

\FloatBarrier
\begin{figure}[!ht]
\centering
\caption{Quadro sinóptico das atividades relacionando ferramentas tecnológicas às tensões encontradas em \aclp{CoP} \citep{Wenger2005}.}
\includegraphics[width=0.65\textwidth,natwidth=600,natheight=600]{imagens/quadroAtividadesFerramentas.png}
\label{quadroAtividadesFerramentas}
\end{figure}
\FloatBarrier

O gerenciamento das relações entre as ferramentas de interação necessárias para colaboração nas \aclp{CoP} visa atender as necessidades de manutenção da comunidade. O uso das ferramentas tecnológicas tradicionais necessita de especificações orientadas aos estilos de comunidades. As ferramentas essenciais para para a gestão das \aclp{CoP} são:

{\bf Perfil} - É fundamental que os participantes possam visualizar o perfil dos outros usuários. A partir do perfil é possível verificar experiências e conhecimentos dos indivíduos, o que permite a definição de mapeamentos de especializações e competências comuns.

{\bf Perfil da \acl{CoP}} - A \acl{CoP} deve apresentar alguns elementos estruturantes como moderadores, regras, domínio de interesse e objetivos. Esse perfil é definido pelo moderador que possui acesso diferenciado dos demais participantes.

{\bf Lista de participantes} - Deve ser possível localizar todos os participantes de uma comunidade, observando assim seu Perfil e Domínio de Interesse. O conhecimento mútuo dos participantes contribui para o aumento de sua confiança, estimulando a participação na comunidade.

{\bf Eventos} - É um importante mecanismo para a manutenção das atividades na comunidade. Eventos valorizam a comunicação, os participantes, divulgam resultados e familiarizam os membros, aumentando sua confiança e fortalecendo a comunidade. Os eventos podem ser tanto virtuais como presenciais.

{\bf Níveis de atividade} - Acompanhar a frequência de colaboração permite a identificação do estágio do ciclo de vida da comunidade, bem como seu ritmo atual.

{\bf Histórico} - Deve permitir a pesquisa por registros de interações, que precisam estar associados a seus produtores.

{\bf Convites} - Participantes aprendem e produzem conhecimento a medida que interagem nas comunidades. Convidar outros usuários possibilita à \acl{CoP} crescer, renovar ideias e continuar agregando conhecimento.

{\bf Recomendações} - É necessário um mecanismo de busca e recomendações de interesses envolvidos na comunidade ou conjunto de comunidades que o usuário pertence, isso garante uma participação de diferentes níveis e mantém o canal de diálogo aberto.

As relações existentes entre as ferramentas, experiências e interesses pessoais fazem transparecer o domínio da \acl{CoP}. Através deles é possível a inferência de interesses dos indivíduos para associá-los às comunidades, outros usuários ou temas que apresentam os mesmos interesses.
Estas associações tem o objetivo de favorecer a disseminação do conhecimento, ampliar a abrangência das práticas e aumentar as relações, recomendando novas e fortalecendo as existentes.

Relações interpessoais não formam necessariamente uma \acl{CoP}, mas à medida que condições favoráveis se apresentam, em termos de ferramentas e estrutura, existe uma grande possibilidade que destas relações possam emergir Comunidades de Prática com os mais diversos domínios.


\section{Conclusões sobre o capítulo}



Ao longo do capítulo foram apresentados os elementos estruturantes de uma \acl{CoP}, seu ciclo de vida, orientações e ferramentas que se enquadram a cada um destes estilos. 
Também foi discutido como o conhecimento é produzido e de que maneira as interações colaboram para seu desenvolvimento neste contexto.

A troca de experiência entre os usuários é o principal objetivo do modelo de \aclp{CoP}. 
As ferramentas citadas no decorrer do capítulo proporcionam diferentes níveis de interação entre os usuário e o ambiente e, de todas estas, o trabalho concentra-se nas que incentivam a comunicação entre os participantes. 

%as de criação de conteúdo, onde a informação pode ser categorizada e utilizada em um sistema de recomendações.
