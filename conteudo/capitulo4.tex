\chapter{Plataforma de Comunidades de Prática}\label{cpCOPPLA}

O produto \acl{CoPPLA}\footnote{https://bitbucket.org/amribeiro/communities.practice/} provê uma plataforma genérica de comunidades de prática virtuais, disponibilizando uma série de ferramentas de comunicação e colaboração integradas em um ambiente voltado para o compartilhamento de conhecimento, onde a criação de conteúdo e a manipulação de objetos é flexível e intuitiva.

A plataforma oferece recursos para o gerenciamento da comunidade, possibilitando a criação, o armazenamento e o acesso a seus conteúdos e participantes.
O conjunto de ferramentas de gestão, comunicação e publicação são configuráveis e envolvem a manipulação de textos, imagens, páginas web, links, eventos, fóruns de discussão e espaços para experiências de aprendizagem.
Os usuários tem a habilidade de criar e gerenciar suas comunidades como um espaço para compartilhar conhecimento envolvendo atividades de aprendizagem.

\FloatBarrier
\begin{figure}[!ht]
\centering
\caption{Interface de uma comunidade do \textit{Framework} \ac{CoPPLA}.}
\includegraphics[width=1\textwidth,natwidth=600,natheight=400]{imagens/CoPPLA.png}
\label{CoPPLAPNG}
\end{figure}
\FloatBarrier

\newpage

\section{Tecnologia}

O \textit{framework} \ac{CoPPLA} é desenvolvido utilizando a solução tecnológica \ac{PZP} que disponibiliza um ambiente web para criação rápida e segura de sites, oferecendo diversas funcionalidades, por exemplo, login, restrições de acesso através de \textit{workflows} que definem papeis e permissões aos usuários e mecanismos de criação, armazenamento, indexação e busca de conteúdo.

\subsection{Python}
Python é uma linguagem de programação de alto nível, interpretada, imperativa, orientada a objetos, de tipagem dinâmica e forte criada por Guido van Rossum no início dos anos 90 \citep{PythonLicense}.

Multiplataforma e com licença livre, compatível com a \ac{GPL}, Python prioriza a legibilidade do código e o desenvolvimento rápido através de uma sintaxe clara e concisa
\footnote{Parte da cultura da linguagem gira ao redor de \textit{The Zen of Python} \citep{PythonZen}, um conjunto de 19 aforismos que descrevem a filosofia de Python. Pode-se vê-lo através do comando Python: \newline $>>>$ \textit{import this}}.


A linguagem conta com uma extensa biblioteca padrão e uma série de módulos e frameworks desenvolvidos por terceiros \citep{PythonAbout}, sendo utilizada em uma grande variedade de domínios de aplicação como desenvolvimento web e de jogos, \acp{GUI}, banco de dados e educação \citep{PythonApps}.


\subsection{Zope}

Zope é um servidor de aplicação web, de código aberto, escrito em Python, que oferece uma solução flexível, segura e escalável para criação, gerenciamento e distribuição de conteúdo \citep{ZopeOverview}.

Seu nome é um acrônimo de \acl{Zope}.
Os objetos publicados com Zope são escritos em Python e armazenados em um banco de dados orientado a objetos nativo, \ac{ZODB}.
Também escrito em Python, o \ac{ZODB} persiste os objetos sem a necessidade de um mapeamento para tabelas de um banco relacional, indexando objetos e metadados que podem ser pesquisados por funções do Zope. A persistência dos objetos é transparente, tornando o código mais simples, robusto e legível \citep{Zodb}.

A indexação e pesquisa são responsabilidades do sistema de catálogo ZCatalog, que é composto por índices e metadados. Índices são campos que podem ser pesquisados e metadados são cópias de conteúdos dos objetos, que podem ser acessados sem a necessidade de acessar o próprio objeto \citep{PloneCatalog}.
ZCatalog possui um mecanismo potente de consultas, podendo pesquisar por múltiplos índices, inclusive atribuindo pesos a cada um deles. Como resultado de uma consulta, ZCatalog retorna uma lista de elementos chamados \textit{Brains}. \textit{Brains} são objetos que possuem todos os metadados vinculados àquele item como atributos e métodos para recuperação do objeto e de sua localização. \textit{Brains} são extremamente rápidos pois são criados em tempo de execução, apenas no momento de uma requisição, e não instanciam os próprios objetos, evitando impactos de performance \citep{ZopeSearching}.

Algumas vezes bancos de dados orientados a objetos podem não ser a melhor solução.
A base de dados pode já existir e o Zope deve apenas utilizá-la, a aplicação pode apresentar dados que são melhor representados por modelos relacionais ou os dados precisam ser acessíveis de fora do Zope.
Nestes casos, é possível integrar bancos de dados relacionais ao sistema \citep{Lerner2002}.

Zope também conta com um conjunto de bibliotecas reutilizáveis, \ac{ZTK}, voltadas para o desenvolvimento de aplicações web, um \ac{CMF} para a construção de aplicações de gerenciamento de conteúdo, um mecanismo de \textit{templates} que interpreta fragmentos \ac{ZPT} e \ac{DTML} para gerar páginas \ac{HTML} dinamicamente, suporte a internacionalização - \ac{I18N} e políticas de segurança. Estes componentes são configurados através de arquivos \ac{ZCML} e acessíveis por meio da \ac{ZMI}, uma interface de gestão web.

\subsection{Plone}

Plone é um sistema de gerenciamento de conteúdo (CMS - \acl{CMS}), escrito em Python e que roda sobre o servidor de aplicação Zope. Sua interface é amigável e foi traduzida para mais de 40 idiomas \citep{PloneProduct}. Focado em usuários com pouco conhecimento técnico, seu principal objetivo é facilitar a criação, edição, publicação e distribuição de conteúdo. 

Plone possui um mecanismo completo de registro onde o administrador do site pode adicionar novos usuários, ou o próprio usuário pode efetuar seu cadastro com login e senha, ambos através de interfaces de fácil manuseio. 
Cada usuário pode criar e modificar suas informações pessoais bem como acessar conteúdos publicados no portal.
O controle de acesso a este conteúdo é feito através de um sistema de \textit{workflow}. 

Um \textit{workflow} define os possíveis estados de um objeto, suas transições e as permissões de acesso vinculadas a cada um dos estados \citep{DefinitivePlone}.
As permissões não são definidas para cada um dos usuários e sim para papéis. O benefício desta abordagem é que um usuário pode ter diversos papéis atribuídos simultaneamente, assim como pode receber papéis de forma global ou em um contexto específico \citep{Aspeli2007}.

Um \textit{workflow} pode ser representado através de um grafo dirigido, os nodos são os estados do objeto e as arestas as possíveis transições entre os estados. O workflow padrão para publicação do plone pode ser visto na figura \ref{workflowPlone}.

\FloatBarrier
\begin{figure}[!ht]
\centering
\caption{\textit{Workflow} padrão para publicação de objetos em Plone.}
\includegraphics[width=0.44\textwidth,natwidth=600,natheight=400]{imagens/workflowPlone.png}
\label{workflowPlone}
\end{figure}
\FloatBarrier


Para cada estado do \textit{workflow} são definidos os privilégios de acesso dos papeis. Por exemplo, na Figura \ref{permissoesPlone},  o estado Pendente habilita o acesso às informações do objeto para contribuidores (\textit{Contributor}), editores (\textit{Editor}), administradores (\textit{Manager}), dono do conteúdo (\textit{Owner}), leitores (\textit{Reader}) e revisores (\textit{Reviewer}) através da permissão de acesso a informação de conteúdo (\textit{Access contents information}), mas permite a alteração do conteúdo (\textit{Modify portal content}) apenas aos papéis de administrador e revisor.


\FloatBarrier
\begin{figure}[!ht]
\centering
\caption{Permissões para o estado Pendente do \textit{workflow} de publicação.}
\includegraphics[width=1\textwidth,natwidth=600,natheight=400]{imagens/permissoesPlone.png}
\label{permissoesPlone}
\end{figure}
\FloatBarrier

Com uma comunidade atuante, Plone  estende uma série de pacotes e ferramentas que atendem as mais variadas necessidades e demandas, além disso, por ser um software de código aberto, pode ser facilmente alterado. Novas funcionalidades podem ser incorporadas com a utilização de \textit{Archetypes}, um recurso desenvolvido para a criação de aplicações para Plone, que fornece um método simples para gerar novos conteúdos baseados em definições de esquemas \citep{Archetypes}.


\section{\textit{Framework}}

O \textit{framework} \ac{CoPPLA} utiliza \textit{Archetypes} para estender os tipos de conteúdo já existentes do Plone, podendo ser instalado em qualquer portal com esta tecnologia. O produto conta ainda com um sistema de autenticação e um conjunto de \textit{workflows} que define as permissões de acesso sobre os papéis dos membros da comunidade.

\subsection{Tipos de conteúdo}

Plone disponibiliza a criação de pastas, arquivos, imagens, links, eventos, páginas web e notícias, entretanto, para ter maior controle sobre os tipos de conteúdo disponíveis no ambiente de comunidades de prática virtuais, o \textit{framework} estende estes tipos nativos, vinculando \textit{templates} e \textit{workflows} personalizados. A estilização dos \textit{templates} visa proporcionar aos participantes uma interface que facilite a interação com o ambiente e o \textit{workflow} se encarrega de definir os papéis e seus níveis de interação.

Uma comunidade de prática e seu conteúdo podem apresentar os estados público, restrito, e inativo. Os usuários podem receber os papéis de autenticado, participante e moderador. Uma comunidade ou conteúdo quando públicos podem ser acessados por qualquer usuário, itens restritos são visíveis apenas aos participantes da comunidade e itens inativos visíveis apenas aos moderadores e ao próprio dono.


\subsection{Ferramentas implementadas}


O \textit{framework} \ac{CoPPLA} disponibiliza espaços para a criação de conteúdo e interação entre os participantes através de:


{\bf Calendário} - Onde eventos podem ser criados por participantes e moderadores;

{\bf Acervo} - Armazena o conteúdo geral da comunidade, podem ser inseridos arquivos, imagens, links, páginas e pastas. Utilizado para publicação de produções coletivas;

{\bf Portfólio} - Utilizado para publicação de produções individuais, disponibiliza a criação de arquivos, imagens e páginas;

{\bf Tarefas} - Espaço para entrega de tarefas (em contextos educacionais);

{\bf Fórum de discussão} - Nas comunidades discussões podem surgir em comentários dos itens publicados ou através de um ambiente de conversas que utiliza o produto Ploneboard\footnote{http://plone.org/products/ploneboard};

{\bf Notificações} - Utilizam-se duas formas de notificação nas comunidades. A primeira delas
é um mecanismo  que permite aos moderadores o envio direto de mensagens aos participantes e a segunda é através de um resumo diário das atividades da comunidade enviado por e-mail;

{\bf Perfil dos usuários} - Exibe informações referentes ao usuário bem como seu conteúdo compartilhado;

{\bf Lista de participantes} - Exibe os participantes e moderadores da comunidade, sendo possível a pesquisa e o acesso ao perfil de cada um;

{\bf Nível de atividade} - Ferramenta desenvolvida para verificar o nível de interação nas comunidade, tanto coletivo quanto individual;

{\bf Histórico} - Armazena todo o conteúdo produzido pelos participantes, pode-se pesquisar por conteúdos específicos utilizando filtros como nome, data de criação, quem publicou, temas relacionados, entre outros.

{\bf Convite} - Ferramenta para envio de convites por e-mail, onde é possível anexar mensagens personalizadas.

{\bf Perfil da comunidade} - Através da página inicial de uma comunidade é possível encontrar seu nome, imagem, descrição, lista de participantes e histórico.


\subsection{Temas, usuários e seus relacionamentos}

Temas são termos de categorização do conhecimento, relacionados a cada conteúdo compartilhado, que representam o domínio da comunidade.
No Plone os temas são palavras chaves representadas como entradas do índice \textit{Subject} do ZCatalog. Um objeto pode ser marcado com um ou mais temas e, a partir destas palavras chave, é possível pesquisar por conteúdos relacionados.
Os temas proporcionam maior flexibilidade na pesquisa e categorização de conteúdo do que índices, uma vez que um item pode ser marcado com mais de um tema simultaneamente \citep{ZopeSearching}.

Em um site desenvolvido sobre \ac{PZP} os usuários são objetos armazenados em uma pasta, no nível do Zope, chamada \textit{acl\_users}. Um usuário Zope possui nome, senha e papéis vinculados a ele. Para suportar propriedades adicionais são utilizados produtos externos para gerenciamento de usuários \citep{ZopeSecurity}.
A autenticação e as informações de permissões são gerenciadas pelo \ac{PAS}, um sistema que implementa várias interfaces de autenticação, por exemplo \ac{LDAP} e OpenID. No nível do Plone, o acesso as informações do usuário é realizado através da ferramenta \textit{portal\_membership} que as fornece através de metadados \citep{PloneMembers}.

De forma nativa Plone não oferece ferramentas para o relacionamento direto entre temas e usuários, uma associação só é possível através dos itens publicados, que possuem um criador e podem ser marcados com temas, relacionando assim, os temas dos objetos a seus criadores. No entanto, essa abordagem torna difícil a execução de tarefas como a consulta por todos os temas relacionados a um usuário ou vice-versa. Também não abrange todos os interesses que um usuário pode apresentar, visto que o mesmo pode interessar-se por temas que não fazem parte de suas publicações.


\section{Conclusões sobre o capítulo}

Este capítulo apresentou uma visão geral sobre a plataforma de \aclp{CoP} a ser utilizada neste trabalho. Foram evidenciadas as principais tecnologias envolvidas na construção e compartilhamento do conhecimento neste framework. A ênfase nas ferramentas e na estrutura do conteúdo armazenado serve de base para a delimitação do escopo a ser utilizado na proposta de solução. Serão utilizadas apenas informações de relacionamentos entre participantes e os temas de interesse dos usuários em \aclp{CoP}. Desta forma, este trabalho não utilizará a estrutura completa de informação para concentrar-se apenas nos conteúdos diretamente relacionados aos seus membros criadores. Recomendações de conteúdo ou relações entre perfis, interesses e conteúdos podem ser abordados por sistemas mais robustos e complexos de recomendação, que estão fora do escopo deste trabalho.

Finalmente, a partir do estudo realizado sobre as ferramentas para instrumentalização de \aclp{CoP} e as já implementadas no \textit{framework} \ac{CoPPLA}, identificou-se a necessidade do desenvolvimento de mecanismos de relacionamento entre os interesses e conteúdos do usuário, assim como de relacionamento entre os participantes, promovendo uma maior interação entre os usuários, o que pode contribuir para o aumento de sua confiança e participação na construção do conhecimento.

%A partir do estudo realizado sobre as ferramentas para instrumentalização de \aclp{CoP} e as já implementadas no \textit{framework} \ac{CoPPLA}, percebe-se a necessidade do desenvolvimento de mecanismos que relacionem os interesses do usuário ao conteúdo produzido nas comunidades. O relacionamento entre os participantes também deve ser motivado uma vez que, quanto maior a interação entre os usuários maior sua confiança e participação na construção do conhecimento.

%No capítulo que segue é proposto o desenvolvimento de um sistema de \textit{following} de temas e usuários utilizando a tecnologia \acl{PZP}. Pretende-se também, considerando o \textit{following} uma medida de interesse, gerar recomendações personalizadas.
